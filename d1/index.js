// console.log("Hello World");

// JAVASCRIPT SYNCHRONOUS AND ASYNCHRONOUS
// Javascript in default run in synchronous
/*
	SYNCHRONOUS
	Synchronous codes run in sequence. This means that each operation must wait for the previous one to complete before executing.
	

	ASYNCHRONOUS
	Asynchronous codes run in parallel. This means that an operation can occur while another one is still being processed.

	Asynchronous code execution is often preferable in situation where execution can be blocked. Some examples of this are network requests, long-running calculations, file system operations, etc. Using asynchronous code in the browser ensures he page remains response and the user experience is mostly unaffected.
*/

// Example of Synchronous:
	console.log("Hello World");
	// cosnole.log("Hello Again");  //it will not execute
	for (let i=0; i<=5; i++){
		console.log(i);
	}
	console.log("Hello it's me");

//API
/*
	- API stands for Application Programming Interface
	- An application programming interface is a particular set of codes that allows software programs to communicate with each other
	- An API is the interface through which you access someone else's code or through which someone else's code accesses yours.

	Example:
		Google APIs
			https://developers.google.com/identity/sign-in/web/sign-in
		Youtube API
			https://developers.google.com/youtube/iframe_api_reference
*/


// FETCH API
	console.log(fetch('https://jsonplaceholder.typicode.com/posts'))

/*
	A "promise" is an object that represents the eventual completion (or failure) of an asynchronous and it's resulting value

	A promise is in one of these three states:
	1. Pending:
		- Initial state, neither fulfilled nor rejected
	2. Fulfilled:
		- Operation was successfully completed
	3. Rejected:
		- Operation failed
*/

/*
	- By using .then method, we can now check for the status of the promise
	- The 'fetch' method will return a 'promise' that resolves to be a 'response' object
	- The '.then' method captures the 'response' object and returns another 'promise' which eventually be "resolved" or "rejected"

	SYNTAX:
		fetch('URL')
		.then((response) => {});
*/

	fetch('https://jsonplaceholder.typicode.com/posts')
	.then(response => console.log(response.status));

	//fetching data from URL
	fetch('https://jsonplaceholder.typicode.com/posts')
	//Use the 'json' method from the 'response' object to convert the data retrieved into JSON format to be used in our application
	.then((response) => response.json())
	// Print the converted JSON value from the 'fetch' request
	// Using multiple '.then' method to create a promise chain
	.then((json) => console.log(json));


// 'ASYNC and AWAIT'
/*
	- The "async" and "await" keywords is another approach that can be used to achieve asynchronous codes
	- Used in functions to indicate which portions of code should be waited
	- Creates an asynchronous function
*/

	async function fetchData () {
		// Waits for the 'fetch' method to complete then stores the value in the 'result' variable
		let result = await fetch('https://jsonplaceholder.typicode.com/posts');
		// Result returned by fetch is returned as a promise
		console.log("This is the result of the data:", result);
		// The returned 'response' is an object
		console.log(typeof result);

		// We cannot access the content of the 'response' by directly accessing it's body property
		console.log(result.body);
		let json = await result.json();
		console.log(json);
	}
	fetchData();


// GETTING A SPECIFIC POST
// Retrieves a specific post following the Rest API (retrieve, /post/id:, GET)
// Change the endpoint or (URI)

	fetch('https://jsonplaceholder.typicode.com/posts/1')
	.then((response) => response.json())
	.then((json) => console.log(json));


// POSTMAN
/*
	url: https://jsonplaceholder.typicode.com/posts/1
	method: GET
*/

/*
	CREATING A POST
	SYNTAX:
		fetch('URL', options)
		.then((response) => {})
		.then((response) => {})
*/

// Creates a new post following the REST API (create, /post/:id, POST)
	fetch('https://jsonplaceholder.typicode.com/posts', {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			title: 'New post',
			body: 'Hello World',
			userId: 1
		})
	})
	.then((response) => response.json())
	.then((json) => console.log(json));  //you will see the result into the console browser


	//to test it on POSTMAN
	/*
		url:https://jsonplaceholder.typicode.com/posts
		method: POST
		in body: raw + JSON
			{
			    "title": "My First Blog Post",
				"body": "Hello World",
				"userId": 1,
			    "id": 101
			}

		- click SEND
	*/


// UPDATING A POST (PUT)
	//Updates a specific post following the REST API (update, /post/:id, PUT)
	fetch('https://jsonplaceholder.typicode.com/posts/1', {
		method: 'PUT',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			id: 1,
			title: 'Updated post',
			body: 'Hello Again!',
			userId: 1
		})
	})
	.then((response) => response.json())
	.then((json) => console.log(json));


	//to test it on POSTMAN
	/*
		url:https://jsonplaceholder.typicode.com/posts/1
		method: PUT
		in body: raw + JSON
			{
				"title": "My First Revised Post",
				"body": "Hello there! I revised this a bit.",
				"userId": 1
			}

		- click SEND
	*/


// Update a post using the PATCH method
/*
	- Updates a specific post following the REST API (update, /post/:id, Patch)
	- The difference between PUT and PATCH is the number of properties being changed
	- PATCH updates the parts (used to update a single/several properties)
	- PUT is used to update the whole object
*/

	fetch('https://jsonplaceholder.typicode.com/posts/1', {
		method: 'PATCH',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			title: 'Corrected post'
		})
	})
	.then((response) => response.json())
	.then((json) => console.log(json));

	//to test it on POSTMAN
	/*
		url:https://jsonplaceholder.typicode.com/posts/1
		method: PATCH
		in body: raw + JSON
			{
				"title": "This is my final title."
			}

		- click SEND
	*/


// DELETING A POST
// deleting a specific post following the REST API (delete, /post/:id, DELETE)
	fetch('https://jsonplaceholder.typicode.com/posts/1', {
		method: 'DELETE',
	})

	//to test it on POSTMAN
	/*
		url:https://jsonplaceholder.typicode.com/posts/1
		method: PATCH

		- click SEND
	*/


// FILTERING THE POST
/*
	- The data can be filtered by sending the userID along with the URL
	- Information sent via the URL can be done by adding the question mark symbol (?)
	SYNTAX:
		Individual Parameters:
			'url?parameterName=value'

		Multiple Parameters:
			'url?paramA=valueA&paramB=valueB'
*/
	//Individual Parameters:
	fetch('https://jsonplaceholder.typicode.com/posts?userId=1')
	.then((response) => response.json())
	.then((json) => console.log(json));

	//Multiple Parameters:
	fetch('https://jsonplaceholder.typicode.com/posts?userId=1&userId=2&userId=3')
	.then((response) => response.json())
	.then((json) => console.log(json));


// RETRIEVE COMMENTS OF A SPECIFIC POST
// Retrieving comments for a specific post following the REST API (retrieve, /posts/id:, GET)
	
	fetch('https://jsonplaceholder.typicode.com/posts/1/comments')
	.then((response) => response.json())
	.then((json) => console.log(json));

